package com.netlify.thelusina.umbrella.main;

import android.content.Context;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.main
 * Created by lusinabrian on 08/11/16.
 * Description:
 */

interface MainInteractor {

    interface OnFetchFinishedListener{
        /**Triggered in case of any task errors when fetching data
         * @param message Message to display to user*/
        void onTaskError(String message);

        /**Triggered in case of network errors
         * @param message Message to display to user*/
        void onNetworkError(String message);

        /**Triggered when successful fetch of data is achieved*/
        void onSuccess();
    }

    void fetchForecast(Context context,OnFetchFinishedListener listener);

}
