package com.netlify.thelusina.umbrella.main;

import android.content.Context;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.main
 * Created by lusinabrian on 08/11/16.
 * Description:Implementation of {@link MainPresenter}
 */

class MainPresenterImpl implements MainPresenter, MainInteractor.OnFetchFinishedListener{

    private MainView mainView;
    private Context context;
    private MainInteractor mainInteractor;
    private String location;
    private boolean mTwoPane;

    /**Constructor to initialize the MainPresenter Implementation
     * This is used to communicate with MainAcitity and Interactor
     * @param context Context in which this constructor is called
     * @param mainView View of the MainActivity handling user interaction*/
    MainPresenterImpl(Context context, MainView mainView, String location, boolean mTwoPane){
        this.context = context;
        this.mainView = mainView;
        this.location = location;
        this.mTwoPane = mTwoPane;
        this.mainInteractor = new MainInteractorImpl();
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void onResume() {
/*        if(mainView != null){
            location = Utility.getPreferredLocation( context);
            // update the location in our second pane using the fragment manager
            if (location != null && !location.equals(mLocation)) {
                ForecastFragment ff = (ForecastFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
                if ( null != ff ) {
                    ff.onLocationChanged();
                }
                DetailFragment df = (DetailFragment)getSupportFragmentManager().findFragmentByTag(DETAIL_FRAG_TAG);
                if ( null != df ) {
                    df.onLocationChanged(location);
                }
                mLocation = location;
            }
        }*/

    }

    @Override
    public void onTaskError(String message) {
        if(mainView != null)
            mainView.displayTaskError(message);
    }

    @Override
    public void onNetworkError(String message) {
        if(mainView != null)
            mainView.displayNetworkError(message);
    }

    @Override
    public void onSuccess() {

    }
}
