package com.netlify.thelusina.umbrella.tools;

import com.netlify.thelusina.umbrella.BuildConfig;
import com.netlify.thelusina.umbrella.details.DetailFragment;
import com.netlify.thelusina.umbrella.forecast.ForecastFragment;
import com.netlify.thelusina.umbrella.main.MainActivity;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella
 * Created by lusinabrian on 07/11/16.
 * Description: Constant Class containing fields required in project
 */

public class Constants {
    public static final String OWM_KEY = BuildConfig.OPEN_WEATHER_MAP_API_KEY;
    public static final String DETAIL_FRAG_TAG = DetailFragment.class.getSimpleName();
    public static final String MAIN_ACT_TAG = MainActivity.class.getSimpleName();
    public static final String FORECAST_FRAG_TAG = ForecastFragment.class.getSimpleName();

}
