package com.netlify.thelusina.umbrella.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.sync
 * Created by lusinabrian on 13/11/16.
 * Description: Service which allows the {@link UmbrellaSyncAdapter} framework to access the {@link UmbrellaAuthenticator}
 */

public class UmbrellaAuthenticatorService extends Service {
    private UmbrellaAuthenticator umbrellaAuthenticator;

    @Override
    public void onCreate() {
        //create a new authenticator object
        umbrellaAuthenticator = new UmbrellaAuthenticator(this);
    }

    /**
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.*/
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
       return umbrellaAuthenticator.getIBinder();
    }
}
