package com.netlify.thelusina.umbrella.main;

import android.net.Uri;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.main
 * Created by lusinabrian on 08/11/16.
 * Description: Interface for {@link MainActivity}
 */

interface MainView {

    /**
     * A callback that all activities must
     * implement. This mechanism allows activities to be notified of item
     * selections
     * DetailFragmentCallback for when an item has been selected.
     * @param dateUri Uri of the clicked item
     * @param mTwoPane Whether the current view is 2 pane layout*/
    void onItemSelected(Uri dateUri, boolean mTwoPane);

    /**Display network error when the client has no internet connection
     * @param message Message to display to user*/
    void displayNetworkError(String message);

    /**Message to display to user in case of any errors with fetching data
     * @param message Message to display*/
    void displayTaskError(String message);
}
