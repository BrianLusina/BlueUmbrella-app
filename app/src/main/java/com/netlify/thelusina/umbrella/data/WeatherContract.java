package com.netlify.thelusina.umbrella.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.Time;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.data
 * Created by lusinabrian on 09/11/16.
 * Description: Defines the Columns and table for the weather database
 */

public class WeatherContract {

    /*Name of the entire content provider*/
    public static final String CONTENT_AUTHORITY = "com.netlify.thelusina.umbrella";

    /*use {@link #CONTENT_AUTHORITY} to connect content provider*/
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" +CONTENT_AUTHORITY);

    /*Possible paths appended to base content uri for possible URIs*/
    public static final String PATH_WEATHER = "weather";
    public static final String PATH_LOCATION = "location";

    //normalize all days that go into the database to the start of the Julian day
    //todo: use Gregorian Calender
    public static long normalizeDate(long startDate){
        Time time = new Time();
        time.set(startDate);
        int julianDay = Time.getJulianDay(startDate, time.gmtoff);
        return time.setJulianDay(julianDay);
    }


    /**Defines table contents of location table*/
    public static final class LocationEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_LOCATION).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

        //table name
        public static final String TABLE_NAME = "location";

        //sent to OWM for location query
        public static final String COLUMN_LOCATION_SETTING = "location_setting";

        //human readable location string provided by the API
        public static final String COLUMN_CITY_NAME = "city_name";

        //to uniquely pinpoint location on the map when the map intent is launched,
        // the latitudes and longitudes are stored
        public static final String COLUMN_COORD_LONG = "coord_long";
        public static final String COLUMN_COORD_LAT = "coord_lat";

        public static Uri buildLocationUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**Defines weather content for the weather table*/
    public static final class WeatherEntry implements BaseColumns{
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_WEATHER).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;

        //table name
        public static final String TABLE_NAME = "weather";

        //column with the foreign key into the location table
        public static final String COLUMN_LOC_KEY = "location_id";

        //date stored as milliseconds since the epoch
        public static final String COLUMN_DATE = "date";

        //weather id as returned by api to determing the icon to be used as determined by the API
        public static final String COLUMN_WEATHER_ID = "weather_id";

        //short desc of weather eg. clear 'sky is clear'
        public static final String COLUMN_SHORT_DESC = "short_desc";
        public static final String COLUMN_WEATHER_ICON = "icon";
        public static final String COLUMN_MAIN = "main";

        //min and max temp for the day
        public static final String COLUMN_MAX_TEMP = "max_temp";
        public static final String COLUMN_MIN_TEMP = "min_temp";
        public static final String COLUMN_DAY_TEMP = "day_temp";
        public static final String COLUMN_MORN_TEMP = "morn_temp";
        public static final String COLUMN_EVE_TEMP = "eve_temp";
        public static final String COLUMN_NIGHT_TEMP = "night_temp";

        //humidity is stored as a float representing a percentage
        public static final String COLUMN_HUMIDITY = "humidity";

        // Humidity is stored as a float representing percentage
        public static final String COLUMN_PRESSURE = "pressure";

        //wind speed is stored as a float representing windspeed mph
        public static final String COLUMN_WIND_SPEED = "wind";

        //degrees are meteorological degrees stored as floats
        public static final String COLUMN_DEGREES = "degrees";

        public static Uri buildWeatherUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildWeatherLocation(String locationSetting){
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

        public static Uri buildWeatherLocationWithStartDate(String locationSetting, long startDate){
            long normalizeDate = normalizeDate(startDate);
            return CONTENT_URI.buildUpon()
                    .appendPath(locationSetting)
                    .appendPath(Long.toString(normalizeDate))
                    .build();
        }

        public static Uri buildWeatherLocationWithDate(String locationSetting, long date) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting)
                    .appendPath(Long.toString(normalizeDate(date))).build();
        }

        public static String getLocationSettingFromUri(Uri uri){
            return uri.getPathSegments().get(1);
        }

        public static long getDateFromUri(Uri uri){
            return Long.parseLong(uri.getPathSegments().get(2));
        }

        public static long getStartDateFromUri(Uri uri){
            String dateString = uri.getQueryParameter(COLUMN_DATE);
            if(dateString != null && dateString.length() > 0){
                return Long.parseLong(dateString);
            }else{
                return 0;
            }
        }

    }


}
