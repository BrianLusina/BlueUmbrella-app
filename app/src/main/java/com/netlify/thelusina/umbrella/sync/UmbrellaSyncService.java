package com.netlify.thelusina.umbrella.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.sync
 * Created by lusinabrian on 12/11/16.
 * Description:
 */

public class UmbrellaSyncService extends Service {

    private static final String UMBRELLA_SYNC_SERVICE_TAG = UmbrellaSyncService.class.getSimpleName();
    private static final Object sSyncAdapterLock = new Object();
    private static UmbrellaSyncAdapter umbrellaSyncAdapter = null;

    @Override
    public void onCreate() {
        Log.d(UMBRELLA_SYNC_SERVICE_TAG, "onCreate, UmbrellaSyncService");
        synchronized (sSyncAdapterLock){
            if(umbrellaSyncAdapter == null){
                umbrellaSyncAdapter = new UmbrellaSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return umbrellaSyncAdapter.getSyncAdapterBinder();
    }
}
