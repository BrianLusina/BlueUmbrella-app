package com.netlify.thelusina.umbrella.app;

import android.app.Application;
import android.content.res.Configuration;

import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella
 * Created by lusinabrian on 07/11/16.
 * Description: Application Class to handle global settings of the app
 */

public class UmbrellaApp extends Application {
    private static UmbrellaApp singleton;
    public static UmbrellaApp getInstance(){
        return singleton;
    }

    @Override public void onCreate() {
        super.onCreate();
        //initialize Stetho library
        Stetho.initializeWithDefaults(this);
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...
        singleton = this;
    }

    //Called by the system when the device configuration changes while your component is running.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    //This is called when the overall system is running low on memory, and would like actively running processes to tighten their belts.
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    /**
     * This method is for use in emulated process environments. It will never be called on a production Android device, where processes are removed by simply killing them; no user code (including this callback) is executed when doing so.
     * */
    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
