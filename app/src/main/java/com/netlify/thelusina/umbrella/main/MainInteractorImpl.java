package com.netlify.thelusina.umbrella.main;

import android.content.Context;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.main
 * Created by lusinabrian on 08/11/16.
 * Description: Implementation of {@link MainInteractor}
 */

class MainInteractorImpl implements MainInteractor {

    @Override
    public void fetchForecast(Context context, OnFetchFinishedListener listener) {

    }
}
