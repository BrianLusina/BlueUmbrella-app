package com.netlify.thelusina.umbrella.forecast;

import android.content.Context;

import com.netlify.thelusina.umbrella.adapters.ForecastAdapter;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.forecast
 * Created by lusinabrian on 08/11/16.
 * Description:Implementation of interface {@link ForecastPresenter}
 */

class ForecastPresenterImpl implements ForecastPresenter {
    private Context context;
    private ForecastAdapter forecastAdapter;
    private ForecastView forecastView;

    /**Constructor for ForecastPresenter implementation
     *@param context Context in which this constructor is build
     * @param forecastAdapter Adapter to build up the forecast in a ListView*/
    ForecastPresenterImpl(Context context, ForecastView forecastView, ForecastAdapter forecastAdapter){
        this.context = context;
        this.forecastView = forecastView;
        this.forecastAdapter = forecastAdapter;
    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        forecastView = null;
    }
}
