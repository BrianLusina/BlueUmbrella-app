package com.netlify.thelusina.umbrella.forecast;

import com.netlify.thelusina.umbrella.adapters.ForecastAdapter;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.forecast
 * Created by lusinabrian on 08/11/16.
 * Description:
 */

interface ForecastInteractor {
    interface OnFinishedListener {
        /**when finished loading set the items to listview*/
        void onFinished(ForecastAdapter forecastAdapter);
    }
}
