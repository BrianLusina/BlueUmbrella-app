package com.netlify.thelusina.umbrella.forecast;

import com.netlify.thelusina.umbrella.adapters.ForecastAdapter;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.forecast
 * Created by lusinabrian on 08/11/16.
 * Description:View interface for {@link ForecastFragment}
 */

interface ForecastView {

    /**Sets the adapter to the {@link ForecastFragment#mListView}
     * @param mForecastAdapter The Forecast adapter to be set to the ListView*/
    void setAdapter(ForecastAdapter mForecastAdapter);

}
