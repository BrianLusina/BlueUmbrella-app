package com.netlify.thelusina.umbrella.forecast;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.forecast
 * Created by lusinabrian on 08/11/16.
 * Description: Presenter for {@link ForecastFragment}
 */

interface ForecastPresenter {

    /**Handles click events of items in the listView*/
    void onItemClicked(int position);

    void onResume();

    void onDestroy();
}
