package com.netlify.thelusina.umbrella.main;

/**
 * Umbrella
 * com.netlify.thelusina.umbrella.main
 * Created by lusinabrian on 08/11/16.
 * Description:Presenter class as middlemane between th {@link MainView} and Model Layer
 */

interface MainPresenter {

    void onDestroy();

    void onResume();
}
