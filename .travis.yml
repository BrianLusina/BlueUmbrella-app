language: android
sudo: false
jdk: oraclejdk8

env:
  matrix:
    # re-executes the script for each Android API
    -ANDROID_TARGET=android-16 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-17 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-18 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-19 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-20 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-21 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-22 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-23 ANDROID_ABI=armeabi-v7a
    -ANDROID_TARGET=android-24 ANDROID_ABI=armeabi-v7a

  global:
    # wait up to 10 minutes for ADB to connect to emulator
    - ADB_INSTALL_TIMEOUT=10

android:
  components:
    - platform-tools
    - tools

    # build tools used
    - build-tools-24.0.2

    # SDK version used for project
    - android-24

    # Additional components
    - extra-google-m2repository
    - extra-android-m2repository
    - extra-android-support

    # Specify at least one system image,
    # if you need to run emulator(s) during your tests
    - sys-img-armeabi-v7a-android-16
    - sys-img-armeabi-v7a-android-17
    - sys-img-armeabi-v7a-android-18
    - sys-img-armeabi-v7a-android-19
    - sys-img-armeabi-v7a-android-20
    - sys-img-armeabi-v7a-android-21
    - sys-img-armeabi-v7a-android-22
    - sys-img-armeabi-v7a-android-23
    - sys-img-armeabi-v7a-android-24

  licenses:
    - android-sdk-license-.+

# specify the directories to cache
cache:
  directories:
    - $HOME/.gradle/caches/2.14.1
    - $HOME/.gradle/caches/jars-1
    - $HOME/.gradle/daemon
    - $HOME/.gradle/native
    - $HOME/.gradle/wrapper
addons:
  apt_packages:
    - pandoc
  artifacts:
    paths:
      - $(git ls-files -o | grep build/outputs | tr "\n" ":")

before_install:
  - pip install --user codecov

# to execute android instrumentation tests, we will need an emulator
# this creates an emulator and tests it without a screen, skin, audio
before_script:
  - echo no | android create avd --force --name test --target $ANDROID_TARGET --abi $ANDROID_ABI
  - emulator -avd test -no-skin -no-audio -no-window &

# executes the task that run the tests
# The 2 first lines compiles the project and the androidTest variant.
# This is going to create 2 different apks
# –continue flag tells gradle to continue if there is any error in the unit test, so we can get the full error report.
# wait for emulator to launch
# print the available devices
# send the key 'menu' to unlock it
# run the tests
script:
  - ./gradlew assembleDebug -PdisablePreDex
  - ./gradlew assembleAndroidTest -PdisablePreDex
  - ./gradlew check -PdisablePreDex --continue --stacktrace
  - android-wait-for-emulator
  - adb devices
  - adb shell input keyevent 82 &amp;
  - adb install -r app/build/outputs/apk/app-debug.apk
  - ./gradlew connectedAndroidTest -PdisablePreDex --continue --stacktrace

after_success:
  - ./gradlew jacocoFullReport
  - codecov
  - bash <(curl -s https://codecov.io/bash)

# If the build fails, Imgur script uploads an image of the current status of the emulator,
# it can be useful to debug and I also print the testing reports.

after_failure:
  - wget http://imgur.com/tools/imgurbash.sh
  - chmod a+x imgurbash.sh
  - adb shell screencap -p | sed 's/\r$//' &gt; screen.png
  - ./imgurbash.sh screen.png
  - pandoc builder/build/reports/tests/index.html -t plain | sed -n '/^Failed tests/,/default-package/p'

after_script:
  # print lint results details
  - for f in app/build/outputs/lint-results.html; do pandoc $f -t plain; done
  - for f in data/build/outputs/lint-results.html; do pandoc $f -t plain; done