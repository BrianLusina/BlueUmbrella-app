[![Codacy Badge](https://api.codacy.com/project/badge/Grade/fb0a65f1b547419f8366d471b0f5c208)](https://www.codacy.com/app/BrianLusina/BlueUmbrella-app?utm_source=github.com&utm_medium=referral&utm_content=BrianLusina/BlueUmbrella-app&utm_campaign=badger)
[![Build Status](https://www.bitrise.io/app/451ab8c10e00a660.svg?token=shtUxJEASxMDy-8wt4-28g)](https://www.bitrise.io/app/451ab8c10e00a660)

Umbrella is a simple Android weather app build with simplicity with a focus on complex layout that is both Material and fun to use.

### Open Weather Map API Key is required.

In order for the Umbrella app to function properly, an API key for openweathermap.org must be included with the build.

It is recommended that you obtain a Key via the following [instructions](http://openweathermap.org/appid#use), and include the unique key for the build by adding the following line to [USER_HOME]/.gradle/gradle.properties

`MyOpenWeatherMapApiKey="<UNIQUE_API_KEY">`

